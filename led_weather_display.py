import RPi.GPIO as GPIO
import pyowm
import re
from time import sleep

# Assign GPIO pin names
WEAR_SHORTS_GREEN = 7
WEAR_SHORTS_RED = 11
NO_COAT_GREEN = 16
NO_COAT_RED = 18
WET_BLUE = 13
STUPID_COLD_BLUE = 15
WINDY_YELLOW = 22
QUESTIONABLE_YELLOW = 32

# Setup GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(WEAR_SHORTS_GREEN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(WEAR_SHORTS_RED, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(NO_COAT_GREEN, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(NO_COAT_RED, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(WET_BLUE, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(STUPID_COLD_BLUE, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(WINDY_YELLOW, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(QUESTIONABLE_YELLOW, GPIO.OUT, initial=GPIO.LOW)

# Initialize OpenWeatherMap
owm = pyowm.OWM('8c32edcd636d52401c17c11d79e08aa5')
DENVER_OWM_ID = 5419384

# Settings
TIME_BETWEEN_UPDATES = 60 * 60 * 1000 # 1 hour
MAX_HOUR_FOR_DAY = 18
WEAR_SHORTS_TEMP_THRESHOLD = 70
NO_COAT_TEMP_THRESHOLD = 50
STUPID_COLD_THRESHOLD = 20
WIND_THRESHOLD = 30
QUESTIONABLE_THRESHOLD = 40

def get_mst_hour_from_gmt_iso_time(gmt_iso_time):
    m = re.search('\d{4}-\d{2}-\d{2} (\d{2})', gmt_iso_time)
    adjusted_time = int(m.group(1)) - 7
    if adjusted_time < 0:
        adjusted_time += 24
    return adjusted_time

def get_updated_daily_forecast():
    forecaster = owm.three_hours_forecast_at_id(DENVER_OWM_ID)
    forecast = forecaster.get_forecast()
    current_weather = owm.weather_at_id(DENVER_OWM_ID).get_weather()
    weather_for_day = [current_weather]
    for i in xrange(8):
        weather = forecast.get(i)
        weather_for_day.append(weather)
        hour = get_mst_hour_from_gmt_iso_time(weather.get_reference_time('iso'))
        if hour + 3 > MAX_HOUR_FOR_DAY:
            break
    return weather_for_day

def update_leds_based_on_forecast(daily_forecast):
    temps = [ w.get_temperature('fahrenheit')['temp'] for w in daily_forecast ]
    min_temp = min(temps)
    max_temp = max(temps)
    max_wind = max([ w.get_wind('miles_hour')['speed'] for w in daily_forecast ])
    questionable = max_temp - min_temp > QUESTIONABLE_THRESHOLD
    is_wet = False
    for w in daily_forecast:
        if (len(w.get_rain()) > 0 or len(w.get_snow()) > 0):
            is_wet = True
            break

    print("Min Temp: " + str(min_temp))
    print("Max Wind: " + str(max_wind))
    print("Wet: " + str(is_wet))
    print("Questionable: " + str(questionable))
    
    if min_temp < WEAR_SHORTS_TEMP_THRESHOLD:
        GPIO.output(WEAR_SHORTS_GREEN, GPIO.LOW)
        GPIO.output(WEAR_SHORTS_RED, GPIO.HIGH)
    else:
        GPIO.output(WEAR_SHORTS_GREEN, GPIO.HIGH)
        GPIO.output(WEAR_SHORTS_RED, GPIO.LOW)

    if min_temp < NO_COAT_TEMP_THRESHOLD:
        GPIO.output(NO_COAT_GREEN, GPIO.LOW)
        GPIO.output(NO_COAT_RED, GPIO.HIGH)
    else:
        GPIO.output(NO_COAT_GREEN, GPIO.HIGH)
        GPIO.output(NO_COAT_RED, GPIO.LOW)

    if is_wet:
        GPIO.output(WET_BLUE, GPIO.HIGH)
    else:
        GPIO.output(WET_BLUE, GPIO.LOW)

    if min_temp <= STUPID_COLD_THRESHOLD:
        GPIO.output(STUPID_COLD_BLUE, GPIO.HIGH)
    else:
        GPIO.output(STUPID_COLD_BLUE, GPIO.LOW)

    if max_wind >= WIND_THRESHOLD:
        GPIO.output(WINDY_YELLOW, GPIO.HIGH)
    else:
        GPIO.output(WINDY_YELLOW, GPIO.LOW)

    if questionable:
        GPIO.output(QUESTIONABLE_YELLOW, GPIO.HIGH)
    else:
        GPIO.output(QUESTIONABLE_YELLOW, GPIO.LOW)

# Main
while True:
    daily_forecast = get_updated_daily_forecast()
    update_leds_based_on_forecast(daily_forecast)
    sleep(TIME_BETWEEN_UPDATES)
